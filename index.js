const express = require('express')
const app = express()
const port = process.env.PORT || 8089
const postgree = require('./src/db/postgree');
const userDB = require('./src/db/user');

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

//TEMP BYPASS CORS
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//LOGIN

app.post('/login', async (req, res) => {
    try {
        if (req.body) {
            let { username, password } = req.body || {};
            if (!!username && !!password) {
                let result = await userDB.login(username,password);
                errorHandler(req,res).ok(result);
            } else {
                errorHandler(req,res).forbiden('Invalid username or password');
            }
        } else {
            errorHandler(req,res).notFound('Invalid body');
        }
    } catch (error) {
        console.log(`Login error: ${error}`);
        errorHandler(req,res).notFound('Invalid username or password');
    }
});

//REGISTER
app.post('/register', async (req, res) => {
    try {
        if (req.body) {
            let { username, password } = req.body || {};
            if (!!username && !!password) {
                let result = await userDB.register(username,password);
                errorHandler(req,res).ok(result);
            } else {
                errorHandler(req,res).forbiden('Invalid username or password');
            }
        } else {
            errorHandler(req,res).invalid('Invalid body');
        }
    } catch (error) {
        console.log(`Register error: ${error}`);
        errorHandler(req,res).invalid(error);
    }
});

app.get('/getAllUser', async (req, res) => {
    try {
        let result = await userDB.getAllUser();
        errorHandler(req,res).ok(result);
    } catch (error) {
        console.log(`Register error: ${error}`);
        errorHandler(req,res).invalid(error);
    }
});
app.get('/delete/:id', async (req, res) => {
    try {
        let { id } = req.params || {};
        if(!!id) {
            let result = await userDB.deleteUser(id);
            console.log(result);
            errorHandler(req,res).ok(result);
        } else {
            errorHandler(req,res).invalid('Invalid user');
        }
    } catch (error) {
        console.log(`Register error: ${error}`);
        errorHandler(req,res).invalid(error);
    }
});

//ERROR_HANDLER
const errorHandler = (req,res) => {
    return {
        notFound: (error)=>{
            res.status(404).send({error});
        },
        forbiden: (error)=>{
            res.status(403).send({error});
        },
        invalid: (error)=>{
            res.status(422).send({error});
        },
        ok: (body)=>{
            res.status(200).send(body);
        }
    }
}

//ENTRY
app.listen(port, () => {
    postgree.connect();
    console.log(`App running on port ${port}.`)
})