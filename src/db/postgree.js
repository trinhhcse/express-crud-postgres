
const { Client } = require('pg');

const client = new Client({
    user: 'trinhhcse', //DEFAULT OS USER NAME
    host: 'localhost', 
    database: 'trinhhcse',//DEFAULT OS USER NAME
    password: '',//DEFAULT EMPTY
    port: 5432,
});

const connect = async () => {
    console.log("Start connect");
    await client.connect();

    //Init 

    let init_result = await client.query(`CREATE TABLE IF NOT EXISTS users(
        id SERIAL NOT NULL,
        username VARCHAR(255) UNIQUE NOT NULL,
        password VARCHAR (255) NOT NULL,
        PRIMARY KEY (id)
     )`);
    console.log(init_result);
    console.log("Create table successfully");
}


const query = async (query,params = []) => {
    return await client.query(
        query,
        params
    );
}

module.exports = {
    connect,
    query
}