const postgree = require('./postgree');
const login = (username,password) => {
    return new Promise((resolve,reject)=>{
        postgree.query("select * from users where username=$1 and password=$2",[username,password])
        .then(value=>{
            if(value.rows?.[0]) {
                resolve(value.rows?.[0]);
            } else {
                reject("Invalid username or password");
            }
        }).catch(error=>{
            console.log(`Query with username: ${username} password: ${password} error: ${error}`);
            //TODO: Handle error message base on error
            reject("Invalid username or password");
        });
    });
}

const getAllUser = () => {
    return new Promise((resolve,reject)=>{
        postgree.query("select * from users")
        .then(value=>{
            if(value.rows) {
                resolve(value.rows);
            } 
        }).catch(error=>{
            console.log(`getAllUser error: ${error}`);
            //TODO: Handle error message base on error
            reject("Error");
        });
    });
}

const register = (username,password) => {
    return new Promise((resolve,reject)=>{
        postgree.query("insert into users (username, password) VALUES ($1, $2) RETURNING *",[username,password])
        .then(value=>{
            console.log(value)
            if(value.rows?.[0]) {
                resolve(value.rows?.[0]);
            } else {
                reject("Username existed");
            }
        }).catch(error=>{
            console.log(`Register with nusername: ${username} password: ${password} error: ${error}`);
            //TODO: Handle error message base on error
            reject("Username existed");
        });
    });
}

const deleteUser = (id) => {
    return new Promise((resolve,reject)=>{
        postgree.query("delete from users where id=$1",[id])
        .then(value=>{
            console.log(value)
            if(value.rowCount > 0) {
                resolve("Delete successfully");
            } else {
                reject("UserID not existed");
            }
        }).catch(error=>{
            console.log(`Delete user error: ${error}`);
            //TODO: Handle error message base on error
            reject("UserID not existed");
        });
    });
}


module.exports = {
    login,
    register,
    getAllUser,
    deleteUser
}