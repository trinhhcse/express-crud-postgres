# Setup project:
```
yarn
```

# Run project
```
yarn start
```

# Step to expose a api
Step 1: Declare a db query in postgree.js
```
const getAllUser = () => {
    return new Promise((resolve,reject)=>{
        postgree.query("select * from users")
        .then(value=>{
            if(value.rows) {
                resolve(value.rows);
            } 
        }).catch(error=>{
            console.log(`getAllUser error: ${error}`);
            //TODO: Handle error message base on error
            reject("Error");
        });
    });
}
```

Step 2: Expose to api with get method in index.js
```
app.get('/getAllUser', async (req, res) => {
    try {
        let result = await userDB.getAllUser();
        res.status(200).send(result);
    } catch (error) {
        console.log(`Get users error: ${error}`);
        res.status(422).send(error);
    }
});
```